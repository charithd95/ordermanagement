package com.nCinga.services;

import com.nCinga.dao.BuyerDao;
import com.nCinga.dao.OrderDao;

public class BuyerServiceImpl implements BuyerService {
    private BuyerDao buyerDao;
    private OrderDao orderDao;

    public BuyerServiceImpl() {
        this.buyerDao = BuyerDao.getInstance();
        this.orderDao = OrderDao.getInstance();
    }
    public int checkAndAddBuyer(String name){
        if (!(buyerDao.isBuyerValid(name))){
            buyerDao.addBuyer(name);
        }
        return buyerDao.buyerIdFromName(name);
    }

}

