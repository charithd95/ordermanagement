package com.nCinga.dao;


import com.nCinga.Seller;

import java.util.ArrayList;

public class SellerDao {
    private ArrayList<Seller> sellers;
    public static SellerDao sellerDao;
    private SellerDao(){
        this.sellers=new ArrayList<Seller>();
    }
    public static SellerDao getInstance(){
        if (sellerDao==null){
            sellerDao= new SellerDao();
        }
        return sellerDao;
    }
    public boolean isValidSeller(int sellerId){
        for (Seller seller:sellers) {
            if(seller.getSellerId()==sellerId){
                return true;
            }
        }
        return false;
    }
    private void addSeller(String name){
        sellers.add(new Seller("Softlogic"));
        sellers.add(new Seller("Nike Showroom"));
        sellers.add(new Seller("Mont Blanc"));
    }
}
