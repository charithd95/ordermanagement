package com.nCinga;

public class Buyer {
    private int buyerId=0;
    private static int autoIncId=0;
    private final String name;

    public Buyer(String name) {
        this.buyerId = autoIncrementId();
        this.name = name;
    }
    private int autoIncrementId(){
        return ++autoIncId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public String getName() {
        return name;
    }
}
