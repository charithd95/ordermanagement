package com.nCinga.dao;

import com.nCinga.Buyer;
import com.nCinga.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.nCinga.dao.ProductDao.productDao;

public class OrderDao {
    private ArrayList<Order> orders;
    public static OrderDao orderDao;

    private OrderDao() {
        this.orders = new ArrayList<Order>();
    }
    public static OrderDao getInstance(){
        if (orderDao==null){
            orderDao= new OrderDao();
        }
        return orderDao;
    }
    public List<Order> getOrderForBuyer(int buyerId){
        List<Order> output = orders.stream().filter(order -> (order.getBuyerId()==buyerId)).collect(Collectors.toList());
        return output;
    }
    public List<Order> getOrderForProduct(int productId){
        List<Order> output = orders.stream().filter(order -> (order.getProductId()==productId)).collect(Collectors.toList());
        return output;
    }
    public Order getOrderById(int orderId){
        Order output = (Order) orders.stream().filter(order -> (order.getOrderId()==orderId));
        return output;
    }
    public boolean isOrderValid(int orderId){
        if (getOrderById(orderId)!=null){
            return true;
        }
        else return false;
    }
    public void increaseOrderAmount(int orderId,int amount){
        for (Order order:orders) {
            if(order.getOrderId()==orderId){
                order.setOrderedQuantity(order.getOrderedQuantity()+amount);
            }

        }
    }
    public void addOrder(int productId,int buyerId,int orderAmount){
        orders.add(new Order(buyerId,productId,orderAmount));
    }
    public List<Order> getOrdersForSeller(int sellerId) {
        List<Order> resultOrders = orders.stream()
                .filter(order -> (productDao.getSellerIdFromProductId(order.getProductId()) == sellerId)).collect(Collectors.toList());
        return resultOrders;
    }

}
