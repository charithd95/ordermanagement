package com.nCinga;

import java.time.LocalDate;

public class Order {
    private int orderId=0;
    private static int autoIncId=0;
    private final int buyerId;
    private final int productId;
    private int orderedQuantity;
    private final LocalDate orderedDate;

    public Order(int buyerId, int productId, int orderedQuantity) {
        this.orderId = autoIncrementId();
        this.buyerId = buyerId;
        this.productId = productId;
        this.orderedQuantity = orderedQuantity;
        this.orderedDate = LocalDate.now();
    }
    private int autoIncrementId(){
        return ++autoIncId;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public int getProductId() {
        return productId;
    }

    public int getOrderedQuantity() {
        return orderedQuantity;
    }

    public LocalDate getOrderedDate() {
        return orderedDate;
    }

    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }
}
