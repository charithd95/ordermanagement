package com.nCinga;

import com.nCinga.services.BuyerServiceImpl;
import com.nCinga.services.OrderServiceImpl;
import com.nCinga.services.ProductsServiceImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        BuyerServiceImpl buyerService = new BuyerServiceImpl();
        ProductsServiceImpl productService = new ProductsServiceImpl();
        OrderServiceImpl orderService = new OrderServiceImpl();

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter User name: ");
        String name = scanner.nextLine();
        int buyerId=buyerService.checkAndAddBuyer(name);
        System.out.println(".....................................................");
        System.out.println("WELCOME TO STORE");
        System.out.println(".....................................................");
        System.out.println("1. View the products");
        System.out.println("2. Place an Order");
        System.out.println("3. View your orders");
        System.out.println("4. View orders for a seller");
        System.out.println("5. View products from a seller");
        System.out.print("Enter the number of the action: ");
        int selection = scanner.nextInt();
        switch (selection) {
            case 1:
                System.out.println("\nVIEW THE PRODUCTS");
                productService.viewProducts();
                break;
            case 2:
                System.out.println("\nPLACE AN ORDER");
                System.out.print("\nEnter Product ID: ");
                int productId = scanner.nextInt();
                System.out.print("Order Quantity: ");
                int quantity = scanner.nextInt();
                orderService.placeAnOrder(productId, buyerId, quantity);
                break;
            case 3:
                System.out.println("\nVIEW YOUR ORDERS");
                orderService.viewOrdersForBuyer(buyerId);
                break;
            case 4:
                System.out.println("\nVIEW ORDERS FOR A SELLER");
                System.out.print("\nEnter Seller Id: ");
                int sellerId = scanner.nextInt();
                System.out.println();
                orderService.viewOrdersForSeller(sellerId);
                break;
            case 5:
                System.out.println("\nVIEW PRODUCTS BY A SELLER");
                System.out.print("\nEnter Seller Id: ");
                int sellerIdOne = scanner.nextInt();
                orderService.viewOrdersForSeller(sellerIdOne);
                break;
            default:
                System.out.println("INVALID OPERATION...BYE!");
                break;
        }
    }
}
