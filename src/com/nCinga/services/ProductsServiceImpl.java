package com.nCinga.services;

import com.nCinga.dao.ProductDao;

import java.util.ArrayList;

public class ProductsServiceImpl implements ProductsService {
    private ProductDao productDao;

    public ProductsServiceImpl(){
        this.productDao=ProductDao.getInstance();

    }
    public void viewProducts(){
        if(productDao.getProducts().size()!=0){
            productDao.getProducts().forEach(product -> System.out.println(product.getProductName()));
        }
        else System.out.println("No Products Available now");
    }
}
