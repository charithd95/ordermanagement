package com.nCinga.services;

import com.nCinga.Order;
import com.nCinga.Product;
import com.nCinga.dao.BuyerDao;
import com.nCinga.dao.OrderDao;
import com.nCinga.dao.ProductDao;
import com.nCinga.dao.SellerDao;

import java.util.ArrayList;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao;
    private BuyerDao buyerDao;
    private ProductDao productDao;
    private SellerDao sellerDao;
    private int orderAmount;

    public OrderServiceImpl() {
        orderDao=OrderDao.getInstance();
        buyerDao=BuyerDao.getInstance();
        productDao=ProductDao.getInstance();
        sellerDao=SellerDao.getInstance();

    }
    public void placeAnOrder(int productId,int buyerId,int orderAmount){
        if (productDao.isProductValidAndAvailable(productId,orderAmount)){
            orderDao.addOrder(buyerId,productId,orderAmount);
            productDao.decreaseProducts(productId,orderAmount);
        }

    }
    public void viewOrdersForBuyer(int buyerId){
        if (buyerDao.isBuyerValid(buyerId)){
            orderDao.getOrderForBuyer(buyerId).forEach(order -> System.out.println(productDao.getProductName(order.getProductId())+"Order Quantity "+order.getOrderedQuantity()));
        }
    }
    public void viewOrdersForSeller(int sellerId){
        if(sellerDao.isValidSeller(sellerId)){
            orderDao.getOrdersForSeller(sellerId).forEach(order -> System.out.println(order.getOrderId()+"Ordered Amount :"+order.getOrderedQuantity()));
        }
    }
    public void viewProductsForSeller(int sellerId){
        if(sellerDao.isValidSeller(sellerId)){
            productDao.getProductsForSeller(sellerId).forEach(product -> System.out.println(product.getProductName()+"  Available Amount :"+product.getAvailableQuantity()));
        }
    }



}
