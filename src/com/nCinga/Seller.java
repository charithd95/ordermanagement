package com.nCinga;

public class Seller {
    private int sellerId=0;
    private static int autoIncId=0;
    private final String name;

    public Seller(String name) {
        this.sellerId = autoIncrementId();
        this.name = name;
    }
    private int autoIncrementId(){
        return ++autoIncId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public String getName() {
        return name;
    }
}
