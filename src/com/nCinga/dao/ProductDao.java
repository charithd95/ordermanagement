package com.nCinga.dao;

import com.nCinga.Buyer;
import com.nCinga.Order;
import com.nCinga.Product;
import com.nCinga.Seller;
import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductDao {
    private ArrayList<Product> products;
    public static ProductDao productDao;

    private ProductDao() {
        this.products = new ArrayList<Product>();
    }
    public static ProductDao getInstance(){
        if (productDao==null){
            productDao = new ProductDao();
        }
        return productDao;
    }
    private void initStoreItems() {
        products.add(new Product(1,"Toy Car", 550, 40));
        products.add(new Product(2,"Black tshirt", 500, 60));
        products.add(new Product(3,"Perfume" ,300, 10));
    }

    public ArrayList<Product> getProducts() {
        return products;
    }
    public boolean isProductValidAndAvailable(int productId,int amount){
        for (Product product:products) {
            if (product.getProductId()==productId && product.getAvailableQuantity()>=amount){
                return true;
            }

        }
        return false;
    }
    public void decreaseProducts(int productId,int amount){
        for (Product product:products) {
            if(product.getProductId()==productId){
                product.setAvailableQuantity(product.getAvailableQuantity()-amount);
            }
        }
    }
    public void addProducts(int sellerId, String productName, double productPrice, int availableQuantity){
        products.add(new Product(sellerId,productName,productPrice,availableQuantity));
    }
    public String getProductName(int productId){
        for (Product product:products) {
            if (product.getProductId()==productId){
                return product.getProductName();
            }

        }
        return null;
    }
    public List<Product> getProductsForSeller(int sellerId){
        List<Product> output = new ArrayList<>();
        for (Product product:products
             ) {
            if (product.getSellerId()==sellerId){
                output.add(product);
            }

        }
        return output;
    }
    public int getSellerIdFromProductId(int productId){
        if(isProductValidAndAvailable(productId,0)){
            for (Product product:products) {
                if (product.getProductId()==productId){
                    return product.getSellerId();
                }
            }
        }
        return 0;

    }


}
