package com.nCinga;

public class Product {
    private int productId=0;
    private static int autoIncId=0;
    private final int sellerId;
    private final String productName;
    private final double productPrice;
    private int availableQuantity;

    public Product(int sellerId, String productName, double productPrice, int availableQuantity) {
        this.productId =autoIncrementId();
        this.sellerId = sellerId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.availableQuantity = availableQuantity;
    }
    private int autoIncrementId(){
        return ++autoIncId;
    }

    public int getProductId() {
        return productId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public String getProductName() {
        return productName;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }
}
