package com.nCinga.services;

public interface OrderService {
    public void placeAnOrder(int productId,int buyerId,int orderAmount);
    public void viewOrdersForBuyer(int buyerId);
    public void viewOrdersForSeller(int sellerId);
    public void viewProductsForSeller(int sellerId);
}
