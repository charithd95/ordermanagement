package com.nCinga.dao;

import com.nCinga.Buyer;

import java.util.AbstractList;
import java.util.ArrayList;

public class BuyerDao {
    private ArrayList<Buyer> buyers;
    public static BuyerDao buyerDao;

    private BuyerDao() {
        this.buyers = new ArrayList<Buyer>();
    }
    public static BuyerDao getInstance(){
        if (buyerDao==null){
            buyerDao= new BuyerDao();
        }
        return buyerDao;
    }
    public void addBuyer(String name){
        buyers.add(new Buyer(name));

    }
    public Buyer getBuyerDetails(int buyerId){
        for (Buyer buyer:buyers) {
            if (buyer.getBuyerId()==buyerId){
                return buyer;
            }
        }
        return null;
    }
    public boolean isBuyerValid(int buyerId){
        for (Buyer buyer:buyers) {
            if (buyer.getBuyerId()==buyerId){
                return true;
            }
        }
        return false;
    }
    public boolean isBuyerValid(String name){
        for (Buyer buyer:buyers) {
            if (buyer.getName().equals(name)){
                return true;
            }
        }
        return false;
    }
    public int buyerIdFromName(String name){
        int output=0;
        for (Buyer buyer:buyers) {
            if (buyer.getName().equals(name)) {
                output=buyer.getBuyerId();
            }
        }return output;
    }
}
